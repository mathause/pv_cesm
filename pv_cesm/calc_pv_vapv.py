
import numpy as np
import netCDF4 as nc

try:
    import pv_fortran
except ImportError:
    msg = """Need to complie pv_fortran with f2py first. By running:
             
             f2py pv_fortran.f -m pv_fortran -h pv_fortran.pyf
             f2py -c pv_fortran.pyf pv_fortran.f
            """
    print(msg)


__all__ = ['calc_vapv', 'calc_pv']

# ======================================================================


def _get_pressure_(ncf):
    """returns relevant variables to calculate pressure"""
    P0 = ncf.variables['P0']
    PS = ncf.variables['PS'][:]

    # The vertical level-structure of the data must be of the form:
    # p = ak(k) + bk(k) * ps.
    # In CAM the form is:
    # p = hyam * P0 + b_k * ps.
    # Thus

    hyam = ncf.variables['hyam'][:]
    hybm = ncf.variables['hybm'][:]

    a_k = hyam * P0
    b_k = hybm

    return a_k, b_k, PS

# ======================================================================


def calc_vapv(filename, pmin, pmax):
    """calculate VAPV for CESM/ CAM model simulation

        Vertically averaged potential vorticity

        Parameters
        ----------
        filename : string
            filename of the netcdf file containg the variables
        pmin : float
            pressure to start the integration
        pmax : float
            pressure to end the integration

        Returns
        -------
        vapv : array
            Numpy array containing the vertically averaged PV

        ..note::
          The file needs to contain the following variables:
          lat, lon, time, hyam, hybm, P0, PS, U, V, T

    """

    # use context manager
    with nc.Dataset(filename) as ncf:
        return _calc(ncf, pmin, pmax, return_pv=False)


# ----------------------------------------------------------------------

def calc_pv(filename):
    """calculate VAPV for CESM/ CAM model simulation

        Vertically averaged potential vorticity

        Parameters
        ----------
        filename : string
            filename of the netcdf file containg the variables

        Returns
        -------
        pv : array
            Numpy array containing PV.

        ..note::
          The file needs to contain the following variables:
          lat, lon, time, hyam, hybm, P0, PS, U, V, T

    """

    # use context manager
    with nc.Dataset(filename) as ncf:
        return _calc(ncf, pmin=None, pmax=None, return_pv=True)


# ----------------------------------------------------------------------

def _calc(ncf, pmin, pmax, return_pv=False):
    """calculate VAPV or PV: inner function"""

    # load 'constants'
    lon = ncf.variables['lon'][:]
    lat = ncf.variables['lat'][:]

    time = ncf.variables['time'][:]

    a_k, b_k, PS = _get_pressure_(ncf)

    # ==================================================================
    # calculate constants

    LON, LAT = np.meshgrid(lon, lat)

    # the extent of the data
    dmin = [lon.min(), lat.min(), 1, 1]
    dmax = [lon.max(), lat.max(), 1, 1]

    # coriolis parameter
    SIGMA = (2 * np.pi) / (23 * 3600 + 56 * 60 + 4.1)
    f = 2 * SIGMA * np.sin(LAT / 180. * np.pi)

    # cos lat
    cos_lat = np.cos(LAT / 180 * np.pi)

    # rcp = R / c_p
    # where: R gas constant of air, c_p the specific heat of air
    rcp = 287 / 1003.

    # allocate the output variables
    if return_pv:
        # the 3D field may be too big to allocate
        shape = ncf.variables['U'].shape
    else:
        shape = PS.shape

    OUT = np.empty(shape=shape)
    OUT.fill(np.NaN)
    # transpose it to be fortran compliant
    OUT = OUT.T

    n_timesteps = len(time)

    # time loop
    for i in range(n_timesteps):
        print(("{: 4d} of {: 4d}".format(i + 1, n_timesteps)))

        # load time dependent data
        U = ncf.variables['U'][i, :]
        V = ncf.variables['V'][i, :]
        T = ncf.variables['T'][i, :]

        ps = PS[i, :]

        # calculate pressure at each level
        P = (a_k + b_k * ps[np.newaxis, :].T).T

        # calculate potential temperature at each level
        TH = T * (100000 / P) ** rcp

        # calculate PV
        # fortran expects a different order of the dimensions
        # pvf.potvort expects pressure in hPa
        pv = pv_fortran.potvort(U.T, V.T, TH.T, ps.T / 100, cos_lat.T,
                                f.T, a_k / 100, b_k, dmin, dmax)

        if return_pv:
            OUT[:, :, :, i] = pv
        else:
            # vertically integrate PV
            # we need to invert the vertical levels
            OUT[:, :, i] = pv_fortran.vint(pv[:, :, ::-1], ps.T,
                                           pmin, pmax,
                                           a_k[::-1], b_k[::-1])

    # transpose back to python dimension ordering
    return OUT.T

# ======================================================================
