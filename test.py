
import pv_cesm

import numpy as np
from pkg_resources import resource_filename
from subprocess import call
import os
import netCDF4 as nc
from docopt import docopt

def test():
    """
    Test if vertically averaged PV is 'similar' in the python/ fortran and NCL.

    Usage:
      get_data.py [<ifile>]
      get_data.py -h | --help
      get_data.py -v | --version

    Arguments:
      <ifile>   Name of file to compare VAPV. If not given uses file under
                pv_cesm/tst_data/tst.nc 

    Options:
      -h --help     Show this screen.
      -v --version  Show version.
    """
    
    arg = docopt(test.__doc__, version='1.0.0')

    # get filenames
    ifile = arg.get('<ifile>', None)
    if ifile is None:
        ifile = resource_filename('pv_cesm', 'tst_data/tst.nc')

    ofile = resource_filename('pv_cesm', 'tst_data/vapv_ncl.nc')

    # ncl does not overwrite files
    try:
        os.remove(ofile)
    except OSError:
        pass

    # calc vapv with python/ fortran
    vapv_py = pv_cesm.calc_vapv(ifile, 15000, 50000)


    # calc vapv with NCL
    load_module = "module load ncl/6.3.0; "
    cmd = """ncl 'ifile = "{ifile}"' 'ofile="{ofile}"' NCL/VAPV.ncl"""
    cmd = cmd.format(ifile=ifile, ofile=ofile)
    call(load_module + cmd, shell=True)

    # load ncl data form netCDF file
    with nc.Dataset(ofile) as ncf:
        vapv_ncl = ncf.variables['vapv'][:]

    # =========================================================================
    # tests
    # =========================================================================

    print("Comparing VAPV of python/ fortran to ncl computiation:")

    txt = "Median difference smaller {} PVU"
    cond = 1e-3
    value = np.abs(np.median((vapv_py - vapv_ncl))) < cond
    test_txt(txt, value, cond)

    txt = "Median absolute difference smaller {} PVU"
    cond = 0.1
    value = np.abs(np.median(np.abs((vapv_py - vapv_ncl)))) < cond
    test_txt(txt, value, cond)

    txt = "Max absolute difference smaller {} PVU"
    cond = 0.6
    value = np.abs(np.max(np.abs((vapv_py - vapv_ncl)))) < cond
    test_txt(txt, value, cond)

    txt = "Minimum time-correlation larger {}"
    cond = 0.95
    value = np.corrcoef(vapv_ncl.reshape(4, -1), vapv_py.reshape(4, -1)).min() > cond
    test_txt(txt, value, cond)

    txt = "Median relative difference smaller {} %"
    cond = 4
    value = np.median((vapv_py - vapv_ncl) / vapv_ncl) * 100 < cond
    test_txt(txt, value, cond)

    txt = "RMSE smaller {} PVU"
    cond = 0.1
    value = np.sqrt(np.mean((vapv_py - vapv_ncl) ** 2)) < cond
    test_txt(txt, value, cond)


def test_txt(txt, passed, cond):
    txt = txt.format(cond)
    passed = 'Yes' if passed else 'No'
    msg = '{:43}: {}'.format(txt, passed)
    print(msg)


if __name__ == '__main__':
    test()
